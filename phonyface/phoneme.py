import logging
from enum import Enum


class PhonemeType(Enum):
    AFFRICATE = 1
    AFFRICATIVE = 2
    ASPIRATE = 3
    FRICATIVE = 4
    LIQUID = 5
    NASAL = 6
    SEMIVOWEL = 7
    STOP = 8
    VOWEL = 9

    @classmethod
    def init_from_name(cls, name):
        for phoneme_type in cls:
            if phoneme_type.name == name.upper():
                return phoneme_type
        raise ValueError(f"No phonemetype with name '{name}' exists")


class Phoneme:
    def __init__(self, symbol: str, kind: PhonemeType):
        self.__symbol: str = symbol
        self.__kind: PhonemeType = kind

    def get_symbol(self) -> str:
        return self.__symbol
    symbol = property(get_symbol)

    def get_kind(self) -> PhonemeType:
        return self.__kind
    kind = property(get_kind)

    def is_vowel(self):
        return self.kind == PhonemeType.VOWEL

    def __eq__(self, other) -> bool:
        if isinstance(other, Phoneme):
            if self.symbol == other.symbol:
                if self.kind == other.kind:
                    return True
                else:
                    logging.warning("Two phonemes were compared and had the same symbol but a different kind. "
                                    "This is very odd, and there might be a logical bug in your code. \n"
                                    f"Guilty phonemes: \n"
                                    f"\t{self}\n"
                                    f"\t{other}")
                    return NotImplemented
        return NotImplemented

    def __hash__(self):
        return hash((self.symbol, self.kind))

    def __str__(self) -> str:
        return self.symbol

    def __repr__(self) -> str:
        return f"Phoneme {self.symbol} [{self.kind}]"
