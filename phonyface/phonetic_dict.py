import abc
import logging
import unicodedata
from collections import UserDict, defaultdict
from typing import Mapping, Sequence, Tuple, Collection, Iterable

from regex import regex

from phonyface.pronunciation import Pronunciation


class PhoneticDictMixin(abc.ABC):
    @abc.abstractmethod
    def get_pronunciation(self, word: str, default=None) -> Pronunciation:
        pass

    @abc.abstractmethod
    def get_pronunciations(self, word: str, default=None) -> Iterable[Pronunciation]:
        pass


class PhoneticDict(UserDict, PhoneticDictMixin):

    # DEV NOTE: overriding the __init__ can probably be done cleaner, as I believe other ways to pass the data
    #   are also accepted. Reconsider if this ever becomes relevant.
    def __init__(self, data: Mapping[str, Sequence[Pronunciation]]):
        super().__init__(data)  # DEV NOTE: passes through __setitem__ for each element of data

    @staticmethod
    def check_entry(key: str, value: Sequence[Pronunciation]) -> bool:
        if key is None or len(key) == 0:
            raise ValueError("Cannot add an entry unless its look-up word has at least 1 character\n"
                             f"Key: {key}, value: {value}")

        if value is None or len(value) == 0:
            raise ValueError("Cannot add an entry unless it has at least 1 pronunciation\n"
                             f"Key: {key}, value: {value}")

        return True

    def __setitem__(self, key: str, value: Sequence[Pronunciation]):
        if self.check_entry(key, value):
            clean_word = self._clean_word(key)
            if clean_word != key:
                logging.info(f"'{key}' was normalised to '{clean_word}' before adding it to the dictionary'")
            super().__setitem__(clean_word, tuple(value))

    def get_pronunciation(self, word, default=None) -> Pronunciation:
        """Returns the first pronunciation of the word, or the default if there are none"""
        pronunciations = self.get_pronunciations(word)
        if len(pronunciations) == 0:
            return default
        else:
            return pronunciations[0]

    def get_pronunciations(self, word, default=tuple()) -> Tuple[Pronunciation]:
        """Returns all pronunciations for the given word, or the default if there are none"""
        clean_word = self._clean_word(word)
        if clean_word != word:
            logging.info(f"'{word}' was normalised to '{clean_word}' before dictionary look-up'")
        return self.get(self._clean_word(word), default=default)

    @staticmethod
    def _clean_word(word):
        space_cleaned = regex.sub(r"(^\s+)|(\s+$)", "", word)  # ToDo investigate whether strip() is equally good
        case_cleaned = space_cleaned.casefold()
        accent_cleaned = unicodedata.normalize('NFC', case_cleaned)
        return accent_cleaned


class ReversePhoneticDict(UserDict):

    def __init__(self, data: Mapping[Pronunciation, Sequence[str]]):
        super().__init__(data)

    def __setitem__(self, key: Pronunciation, value: Sequence[str]):
        if self.check_entry(key, value):
            super().__setitem__(key, tuple(value))

    @staticmethod
    def check_entry(key: Pronunciation, value: Sequence[str]) -> bool:
        if key is None:
            raise ValueError("Cannot add an entry without pronunciation")

        if value is None or len(value) == 0:
            raise ValueError("Cannot add an entry unless it has at least 1 word\n"
                             f"Key: {key}, value: {value}")
        return True

    def get_words(self, pronunciation: Pronunciation, default=tuple()) -> Tuple[str]:
        return self.get(pronunciation, default=default)

    def get_word(self, pronunciation, default=None) -> str:
        res = self.get_words(pronunciation)
        if len(res) > 0:
            return res[0]
        return default

    @classmethod
    def init_from_phonetic_dict(cls, phonetic_dict: PhoneticDict) -> "ReversePhoneticDict":
        reverse_dict_content = defaultdict(list)
        for word, pronunciations in phonetic_dict.items():
            for pronunciation in pronunciations:
                reverse_dict_content[pronunciation].append(word)
        return cls(reverse_dict_content)


