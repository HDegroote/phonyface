from typing import Iterable, List, Tuple
from g2p_en import G2p
from regex import regex
from phonyface.interfaces.cmu_interface import CMUDictParser
from phonyface.phonetic_dict import PhoneticDictMixin
from phonyface.pronunciation import Pronunciation


class G2PDict(PhoneticDictMixin):
    # DEVNOTE: this code lacks unit tests
    # DEVNOTE: this code has a lot of ugly dependencies on cmu_interface which should be cleaned up
    __PHONEME_DICT = CMUDictParser.parse_phonemes()  # Same phonemeset as CMU dict

    def __init__(self):
        self.engine = G2p()

    def get_pronunciation(self, word: str, default=None) -> Pronunciation:
        res_in_gp2_format = self.engine(word)
        res_in_phony_format = self.transform_gp2_to_phonyface_format(res_in_gp2_format)
        return res_in_phony_format

    def get_pronunciations(self, word: str, default=None) -> Tuple[Pronunciation]:
        return self.get_pronunciation(word),

    @classmethod
    def _clean_word(self, word):
        # Note: assumes the word contains no unwarranted punctuation
        space_cleaned = regex.sub(r"(^\s+)|(\s+$)", "", word) # ToDo Check if this is strip equivalent

    @classmethod
    def transform_gp2_to_phonyface_format(cls, gp2_res: List[str]) -> Pronunciation:
        # DEVNOTE: If this ever throws unwarranted errors, write a proper transformer
        res = CMUDictParser.parse_pronunciation(gp2_res, phonemes_dict=cls.__PHONEME_DICT)
        return res