from pathlib import Path
from typing import Sequence, Dict, Tuple, List, TextIO
from regex import regex
from phonyface import filesys
from phonyface.phoneme import Phoneme, PhonemeType
from phonyface.phonetic_dict import PhoneticDict
from phonyface.pronunciation import Pronunciation, Stress


class CMUDict(PhoneticDict):
    def __init__(self, cmu_main_dir: Path = filesys.CMU_MAINDIR):
        super().__init__(CMUDictParser.parse_dict(cmu_main_dir))
        CMUDictParser.parse_dict(filesys.CMU_MAINDIR)


class DictEntryFactory:
    def __init__(self, spelling: str, pronunciation: Pronunciation):
        self.spelling: str = spelling
        self.__pronunciations: List[Pronunciation] = [pronunciation]

    def add_pronunciation(self, pronunciation: Pronunciation):
        self.__pronunciations.append(pronunciation)

    def get_entry(self) -> Tuple[str, Tuple[Pronunciation]]:
        return self.spelling, tuple(self.__pronunciations, )


class CMUDictParser:
    CMUDICT_FILE_NAME = "cmudict.dict"
    PHONEMES_FILE_NAME = "cmudict.phones"

    @classmethod
    def parse_dict(cls, main_dir: Path = filesys.CMU_MAINDIR) -> Dict[str, List[Pronunciation]]:
        main_dir = Path(main_dir)
        phonemes_dict = cls.parse_phonemes(main_dir)
        with open(main_dir / cls.CMUDICT_FILE_NAME, "r") as cmudict_file_stream:
            entries = cls._read_dict_file(cmudict_file_stream, phonemes_dict)
        return entries

    @classmethod
    def parse_phonemes(cls, main_dir: Path = filesys.CMU_MAINDIR) -> Dict[str, Phoneme]:
        with open(main_dir / cls.PHONEMES_FILE_NAME, "r") as phonemes_file_stream:
            phonemes_dict = cls.read_cmu_phonemes(phonemes_file_stream)
        return phonemes_dict

    @staticmethod
    def read_cmu_phonemes(cmudict_phonemes_raw_content: TextIO) -> Dict[str, Phoneme]:
        lines = cmudict_phonemes_raw_content.readlines()
        phonemes = dict()
        separator_symbol = "\t"
        for line_nr, dirty_line in enumerate(lines):
            line = dirty_line.strip()
            line_content = line.strip().split(separator_symbol)
            if len(line_content) == 2:
                phoneme_txt = line_content[0]
                phoneme_type = PhonemeType.init_from_name(line_content[1])
                phonemes[phoneme_txt] = Phoneme(phoneme_txt, phoneme_type)
        return phonemes

    @staticmethod
    def parse_pronunciation(phonemes_in_word: Sequence[str], phonemes_dict: Dict[str, Phoneme]) -> Pronunciation:
        phoneme_regex = r"(?P<symbol>[^\d]{1,2})(?P<stress>[0,1,2])?$"

        res_phonemes = []
        vowel_stresses = []
        for i, phoneme_in_word in enumerate(phonemes_in_word):
            phoneme_match = regex.match(phoneme_regex, phoneme_in_word)
            if phoneme_match is not None:
                phoneme_name = phoneme_match.group("symbol")
                phoneme = phonemes_dict[phoneme_name]
                res_phonemes.append(phoneme)
                stress = phoneme_match.group("stress")
                if stress is not None:
                    if not phoneme.is_vowel():
                        raise ValueError(f"A stress is defined for a phoneme which is not a vowel: "
                                         f"{phoneme} in {phoneme_match}")
                    vowel_stresses.append(Stress(int(stress)))
                if stress is None and phoneme.is_vowel():
                    raise ValueError(f"No stress was defined for a vowel: "
                                     f"{phoneme} in {phoneme_match}")
            else:
                raise ValueError(f"Cannot parse phoneme {phoneme_in_word}")
        return Pronunciation(res_phonemes, vowel_stresses)

    @classmethod
    def _read_dict_file(cls, cmudict_raw_content: TextIO, phonemes_dict: Dict[str, Phoneme]) -> \
            Dict[str, List[Pronunciation]]:
        sep_symbol = " "
        multiple_entry_indicator_start = "("
        comment_symbol = "#"

        entry_factory = None

        dict_entries = dict()
        lines = cmudict_raw_content.readlines()
        if len(lines) == 0:
            raise ValueError("An empty dict file was passed")

        for dirty_line in lines:
            line = dirty_line.strip()
            spelling_and_phonemes = line.split(sep_symbol)
            spelling = spelling_and_phonemes[0].split(multiple_entry_indicator_start)[0]

            comment_i = len(spelling_and_phonemes)  # base case when no comment
            if comment_symbol in spelling_and_phonemes:
                comment_i = \
                [i for i in range(1, len(spelling_and_phonemes)) if spelling_and_phonemes[i] == comment_symbol][0]

            phonemes_as_text = spelling_and_phonemes[1:comment_i]
            pronunciation = cls.parse_pronunciation(phonemes_as_text, phonemes_dict)

            if entry_factory is None:
                entry_factory = DictEntryFactory(spelling, pronunciation)
            elif entry_factory.spelling == spelling:
                entry_factory.add_pronunciation(pronunciation)
            else:
                new_spelling, new_pronunciations = entry_factory.get_entry()
                dict_entries[new_spelling] = new_pronunciations
                entry_factory = DictEntryFactory(spelling, pronunciation)

        new_spelling, new_pronunciations = entry_factory.get_entry()
        dict_entries[new_spelling] = new_pronunciations

        return dict_entries
