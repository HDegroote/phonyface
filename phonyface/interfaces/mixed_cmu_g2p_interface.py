import logging
from typing import Tuple
from phonyface.interfaces.cmu_interface import CMUDict
from phonyface.interfaces.g2p_interface import G2PDict
from phonyface.pronunciation import Pronunciation


logger = logging.getLogger(__name__)


class CmuDictWithG2pFallback(CMUDict):
    # Note: this code lacks unit tests
    def __init__(self, g2p_dict: G2PDict = G2PDict()):
        super().__init__()
        self.g2p_dict = g2p_dict

    def get_pronunciations(self, word, default=tuple()) -> Tuple[Pronunciation]:
        """Returns all pronunciations for the given word, or the default if there are none.
        Uses the CMU dict's result if it has an entry, and falls back on G2P"""
        default = None
        cmu_res = super().get_pronunciations(word, default=default)
        if cmu_res is default:
            logger.info(f"No entry found in CMU dict for '{word}'. Falling back on g2p model")
            return self.g2p_dict.get_pronunciations(word)
        else:
            return cmu_res
