from enum import Enum
from typing import Iterable, Mapping, Tuple, Sequence

from phonyface.phoneme import Phoneme


class Stress(Enum):
    MAIN = 1
    SECONDARY = 2
    NONE = 0


class StressPattern(tuple):
    # ToDo Check whether this clies complies with the specifications of a sequence type
    @classmethod
    def __new__(cls, *args, **kwargs):
        return super().__new__(*args, **kwargs)

    def __str__(self):
        pattern_with_str = (str(stress.value) for stress in self)
        return "".join(pattern_with_str)

    def __getitem__(self, key):
        # DEV NOTE: this method is overriden because when passed a key, it should return an object of the same
        #   type as self. This is not true with the standard implementation of tuple, which returns a tuple object
        #   (The practical reason to do this is to be able to call the str() method on slices)
        #   Do note that this solution is inefficient, because it creates a new tuple everytime it is caleld
        #   See: https://docs.python.org/3/reference/datamodel.html#object.__getitem__
        # ToDo Evaluate whether better solution exists
        res = super().__getitem__(key)
        if isinstance(res, tuple):  # Key was a range
            return StressPattern(res)
        else:
            return res  # Key was an int


class Pronunciation:
    def __init__(self, phonemes: Sequence[Phoneme], stress_pattern: Sequence[Stress]):
        nr_vowels = [phoneme for phoneme in phonemes if phoneme.is_vowel()]
        if not len(nr_vowels) == len(stress_pattern):
            raise ValueError(f"The amount of vowel phonemes is not equal to the amount of stresses\n"
                             f"\tPhonemes: {phonemes}"
                             f"\tStress pattern: {stress_pattern}")

        self.__phonemes: Tuple[Phoneme] = tuple(phonemes)
        self.__stress_pattern = StressPattern(stress_pattern)

    def get_phonemes(self) -> Tuple[Phoneme]:
        return self.__phonemes
    phonemes = property(get_phonemes)

    def get_vowel_indices(self) -> Tuple[int]:
        return tuple(i for i, phoneme in enumerate(self.phonemes) if phoneme.is_vowel())
    vowel_indices = property(get_vowel_indices)

    def get_stress_pattern(self) -> Tuple[Stress]:
        return self.__stress_pattern
    stress_pattern = property(get_stress_pattern)

    def get_nth_vowel(self, n: int) -> Phoneme:
        """Return the nth vowel. Accepts negative indexing (e.g. -1 => last vowel index)"""
        return self.phonemes[self.vowel_indices[n]]

    def get_vowels(self) -> Tuple[Phoneme]:
        return tuple(phoneme for phoneme in self.phonemes if phoneme.is_vowel())

    def __eq__(self, other) -> bool:
        if isinstance(other, Pronunciation):
            if self.phonemes == other.phonemes and self.stress_pattern == other.stress_pattern:
                return True
        return NotImplemented

    def __hash__(self):
        return hash((self.phonemes, self.stress_pattern))

    def __str__(self) -> str:
        res = ""
        vowel_counter = 0
        str_stress_pattern = str(self.stress_pattern)
        for phoneme in self.phonemes:
            stress = ""
            if phoneme.is_vowel():
                stress += str_stress_pattern[vowel_counter]
                vowel_counter += 1
            res += phoneme.symbol + stress + " "
        return res

    def __repr__(self):
        return f"Pronunciation object: {str(self)}"


class UnknownPronunciation(Pronunciation):
    #ToDo Consider changing to a Singleton to avoid hash collisions
    def __init__(self):
        super().__init__([], [])

    def __str__(self) -> str:
        return "UnknownPronunciation"

    def __repr__(self):
        return f"UnknownPronunciation object"
