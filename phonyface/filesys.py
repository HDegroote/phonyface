import logging
from pathlib import Path

__STATIC_DIR_NAME = "static"

STATIC_DIR = Path(__file__).resolve().parent / __STATIC_DIR_NAME # DEVNOTE: ugly solution
if not STATIC_DIR.is_dir():
    logging.warning(f"Static directory not found at expected location {STATIC_DIR}")

CMU_MAINDIR = STATIC_DIR / "cmudict"
if not CMU_MAINDIR.is_dir():
    logging.warning(f"CMU main directorynot found at expected location {CMU_MAINDIR}")
