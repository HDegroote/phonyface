import logging
from phonyface.interfaces.cmu_interface import CMUDict
from phonyface.interfaces.g2p_interface import G2PDict
from phonyface.interfaces.mixed_cmu_g2p_interface import CmuDictWithG2pFallback


def illustrate_g2p():
    print("G2P is a grapheme to phoneme model which does not rely on a vocabulary."
          " \nThis means that even for out-of-vocabulary or non-existent words, it will "
          "provide a best-effort pronunciation")

    words_of_joyce = ["commodius", "vicus", "of", "recirculation"]
    g2p_dict = G2PDict()

    for joy in words_of_joyce:
        print(f"{joy} -> {g2p_dict.get_pronunciation(joy)}")

    print("\nCompare this with the static cmu_dict, which is rather lost for words at Finnegan's wake: ")
    cmu_dict = CMUDict()
    for joy in words_of_joyce:
        print(f"{joy} -> {cmu_dict.get_pronunciation(joy)}")


    print("\nHowever, a G2P model has its limitations as well")
    mad_joyce_madder_g2p = "weenybeenyveenyteeny"
    print(f"\n{mad_joyce_madder_g2p} -> {g2p_dict.get_pronunciation(mad_joyce_madder_g2p)}")

    print("And as all ML-based methods might do odd things")
    what_ml_is = "unpunctilious"
    print(f"\n{what_ml_is} -> {g2p_dict.get_pronunciation(what_ml_is)}")



    print("\nIt is also possible to combine a dictionary and G2P model"
          ", as in the CmuDictWithG2pFallback interface. ")
    cmu_with_g2p_fallback = CmuDictWithG2pFallback()

    word_in_cmu = "normal"
    print(f"{word_in_cmu} -> {cmu_with_g2p_fallback.get_pronunciation(word_in_cmu)}")

    word_not_in_cmu = "cluelessness"
    print(f"{word_not_in_cmu} -> {cmu_with_g2p_fallback.get_pronunciation(word_not_in_cmu)}")


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    illustrate_g2p()