# PHONYFACE
## An object-oriented interface for phonetic dictionaries

Features include:
* Look up pronunciations 
* Look up words corresponding to a pronunciation 
* Find the stress pattern (~rhythm) of a pronunciation
* Look up the words corresponding to a pronunciation

Interfaces:
* [CMU](http://www.speech.cs.cmu.edu/cgi-bin/cmudict/) pronouncing dictionary
* [G2Pe](https://github.com/HDegroote/g2p)  grapheme to phoneme model (forked from [here](https://github.com/Kyubyong/g2p))
* CMU pronouncing dictionary with G2Pe fallback
* Reverse CMU dictionary (pronunciation-> words)

### Installation instructions
This code was built with poetry. To install the dependencies:

    poetry install

Alternatively:

    pip install -r requirements.txt

### Examples

    python CMU_example.py
    python poem_analysis_example.py
    python g2p_interface_illustrations.py

### Usage example 



Load the dictionary (this takes a few seconds)

    cmu_dict = CMUDict()

Look up look and up:

    cmu_dict.get_pronunciation("look") 
	 ... L UH1 K 
	
    cmu_dict.get_pronunciation("up")
     ... AH1 P 

Get all pronunciations of an example which has more than one:
    
    cmu_dict.get_pronunciations("an")
     ... (Pronunciation object: AE1 N , Pronunciation object: AH0 N )

Play around with more of the same (homophones)

    reverse_dict = ReversePhoneticDict.init_from_phonetic_dict(cmu_dict)
    pronunciation_same = cmu_dict.get_pronunciation("same")
    reverse_dict.get_words(pronunciation_same)
     ... ('same', 'sejm')

Working with stress

Get the available stresses:

    for stress in Stress:
        print("\t", stress, "->", stress.value)
     ... Stress.MAIN -> 1
	     Stress.SECONDARY -> 2
	     Stress.NONE -> 0

Get the stress pattern of pattern 

    cmu_dict.get_pronunciation("pattern").stress_pattern
     ... 10

