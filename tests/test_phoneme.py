import unittest

from phonyface.phoneme import Phoneme, PhonemeType


class TestPhonemeType(unittest.TestCase):
    def test_init_from_string_normal(self):
        self.assertEqual(PhonemeType.init_from_name("vowel"), PhonemeType.VOWEL)
        for phoneme_type in PhonemeType:
            self.assertEqual(PhonemeType.init_from_name(phoneme_type.name), phoneme_type)

    def test_init_from_string_invalid(self):
        with self.assertRaises(ValueError):
            PhonemeType.init_from_name("Nope")

class TestPhoneme(unittest.TestCase):
    def setUp(self):
        self.symbol = "AA"
        self.kind = PhonemeType.VOWEL
        self.phoneme = Phoneme(symbol=self.symbol,
                               kind=self.kind)

    def test_init(self):
        self.assertEqual(self.phoneme.symbol, self.symbol)
        self.assertEqual(self.phoneme.kind, self.kind)

    def test_eq_positive(self):
        other = Phoneme(self.symbol, self.kind)
        self.assertEqual(self.phoneme, other)

    def test_eq_negative(self):
        other = Phoneme("AE", self.kind)
        self.assertNotEqual(other, self.phoneme)

    def test_hash(self):
        equal_phoneme = Phoneme(self.symbol, self.kind)
        self.assertEqual(self.phoneme, equal_phoneme)  # Sanity check
        my_phonemes = {self.phoneme, self.phoneme}
        self.assertEqual(len(my_phonemes), 1)

    def test_is_vowel_pos(self):
        self.assertTrue(self.phoneme.is_vowel())

    def test_is_vowel_neg(self):
        self.phoneme = Phoneme(symbol="B",
                               kind=PhonemeType.STOP)
        self.assertFalse(self.phoneme.is_vowel())


if __name__ == '__main__':
    unittest.main()
