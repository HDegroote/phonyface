import copy
import unittest
from collections import MutableSequence, Sequence

from phonyface.phoneme import Phoneme, PhonemeType
from phonyface.pronunciation import Pronunciation, Stress, StressPattern
from tests.data import Data


class TestPronunciation(unittest.TestCase):

    def test_valid_init(self):
        data = Data()
        self.assertEqual(tuple(data.AE_N_phonemes), data.AE_N_pronunciation.phonemes)
        self.assertEqual(tuple(data.AE_N_stresses), data.AE_N_pronunciation.stress_pattern)

    def test_init_deepcopy_phonemes(self):
        data = Data()
        phonemes = list(data.AE_N_phonemes)
        pronunciation = Pronunciation(phonemes, data.AE_N_stresses)
        self.assertTrue(issubclass(type(phonemes), Sequence))
        self.assertFalse(issubclass(type(pronunciation.phonemes), MutableSequence))

    def test_cannot_init_with_vowels_lacking_stress(self):
        data = Data()
        with self.assertRaises(ValueError):
            Pronunciation(phonemes=data.AE_N_phonemes,
                          stress_pattern=[])

    def test_cannot_init_with_stress_sin_corresponding_vowel(self):
        data = Data()
        stresses = list(data.AE_N_stresses)
        stresses.append(Stress.MAIN)
        with self.assertRaises(ValueError):
            Pronunciation(phonemes=data.AE_N_phonemes,
                          stress_pattern=stresses)

    def test_immutable_phonemes(self):
        data = Data()
        phonemes = data.AE_N_pronunciation.phonemes
        self.assertTrue(issubclass(type(phonemes), Sequence))
        self.assertFalse(issubclass(type(phonemes), MutableSequence))

    def test_immutable_stress_pattern(self):
        data = Data()
        stress_pattern = data.AE_N_pronunciation.stress_pattern
        self.assertTrue(issubclass(type(stress_pattern), Sequence))
        self.assertFalse(issubclass(type(stress_pattern), MutableSequence))

    def test_get_vowel_indices(self):
        data = Data()
        indices = data.AE_N_pronunciation.get_vowel_indices()
        self.assertEqual(tuple(indices), (0,))

    def test_eq_pos(self):
        data = Data()
        other = copy.deepcopy(data.AE_N_pronunciation)
        self.assertEqual(other, data.AE_N_pronunciation)

    def test_eq_neg_stress(self):
        data = Data()
        other_stresses = [Stress.SECONDARY]
        other_pron = Pronunciation(phonemes=data.AE_N_phonemes,
                                   stress_pattern=other_stresses)
        self.assertNotEqual(other_pron, data.AE_N_pronunciation)

    def test_eq_neg_phoneme(self):
        data = Data()
        other_phonemes = [data.AH, data.N]
        other_pron = Pronunciation(phonemes=other_phonemes,
                                   stress_pattern=data.AE_N_stresses)
        self.assertNotEqual(other_pron, data.AE_N_pronunciation)

    def test_hash(self):
        data = Data()
        equal_pron = copy.deepcopy(data.AE_N_pronunciation)
        self.assertEqual(data.AE_N_pronunciation, equal_pron)  # Sanity check
        prons = {data.AE_N_pronunciation, data.AE_N_pronunciation}
        self.assertEqual(len(prons), 1)

    def test_str(self):
        data = Data()
        str_repr = str(data.AE_N_pronunciation)
        self.assertIn(data.AE.symbol, str_repr)

    def test_get_nth_vowel(self):
        data = Data()
        self.assertEqual(data.AE_N_AH_pronunciation.get_nth_vowel(0), data.AE)
        self.assertEqual(data.AE_N_AH_pronunciation.get_nth_vowel(1), data.AH)
        self.assertEqual(data.AE_N_AH_pronunciation.get_nth_vowel(-1), data.AH)
        self.assertEqual(data.AE_N_AH_pronunciation.get_nth_vowel(-2), data.AE)

    def test_get_vowels(self):
        data = Data()
        self.assertSequenceEqual(data.AE_N_AH_pronunciation.get_vowels(), (data.AE, data.AH))


class TestStressPattern(unittest.TestCase):
    def setUp(self):
        self.data = Data()

    def test_getitem_range(self):
        sub_pattern = self.data.AE_N_AH_pronunciation.stress_pattern[0:1]
        self.assertIsInstance(sub_pattern, StressPattern)

    def test_getitem_range(self):
        stress = self.data.AE_N_AH_pronunciation.stress_pattern[0]
        self.assertEqual(stress, tuple(self.data.AE_N_AH_pronunciation.stress_pattern)[0])


if __name__ == '__main__':
    unittest.main()
