import copy
import unittest
from collections import MutableSequence, Sequence

from phonyface.phoneme import Phoneme, PhonemeType
from phonyface.phonetic_dict import PhoneticDict, ReversePhoneticDict
from phonyface.pronunciation import Pronunciation, Stress

# TODO add check on repeated values for same key in dict

from tests.data import Data


class TestPhoneticDict(unittest.TestCase):

    def test_valid_init(self):
        data = Data()
        self.assertEqual(data.an_ana_dict[data.an_word], data.an_entry[1])
        self.assertEqual(len(data.an_ana_dict), 2)

    def test_init_deepcopy_init_data(self):
        data = Data()
        orig_dict_content = copy.deepcopy(data.an_ana_dict_content)
        data.an_ana_dict_content["Ow"] = data.ana_entry[1]
        self.assertEqual(orig_dict_content, dict(data.an_ana_dict))
        self.assertNotEqual(data.an_ana_dict_content, dict(data.an_ana_dict))

    def test_word_without_pronunciation_is_invalid(self):
        data = Data()
        data.an_ana_dict_content["Ow"] = None
        with self.assertRaises(ValueError):
            self.phonetic_dict = PhoneticDict(data.an_ana_dict_content)

        data.an_ana_dict_content["Ow"] = tuple()
        with self.assertRaises(ValueError):
            self.phonetic_dict = PhoneticDict(data.an_ana_dict_content)

    def test_empty_word_is_invalid(self):
        data = Data()
        dict_content = {None: (data.AE_N_pronunciation,)}
        with self.assertRaises(ValueError):
            self.phonetic_dict = PhoneticDict(dict_content)

    def test_add_elem(self):
        data = Data()
        self.assertEqual(len(data.an_ana_dict), 2)
        data.an_ana_dict["More"] = (data.AH_N_pronunciation,)
        self.assertEqual(len(data.an_ana_dict), 3)

    def test_add_invalid_elem(self):
        data = Data()
        with self.assertRaises(ValueError):
            data.an_ana_dict["Ow"] = tuple()

    def test_get_pronunciations(self):
        data = Data()
        word, gold_pronunciations = data.an_entry
        pronunciations = data.an_ana_dict.get_pronunciations(word)
        self.assertEqual(gold_pronunciations, pronunciations)

        pronunciations = data.an_ana_dict.get_pronunciations(word, default="Who cares")
        self.assertEqual(gold_pronunciations, pronunciations)

    def test_get_pronunciations_fail_with_default(self):
        data = Data()
        pronunciations = data.an_ana_dict.get_pronunciations("NOPE", default="123")
        self.assertEqual(pronunciations, "123")

    def test_get_pronunciation(self):
        data = Data()
        word, pronunciations = data.an_entry
        gold = pronunciations[0]

        pronunciation = data.an_ana_dict.get_pronunciation(word)
        self.assertEqual(gold, pronunciation)
        pronunciation = data.an_ana_dict.get_pronunciation(word, default="Who cares")
        self.assertEqual(gold, pronunciation)

    def test_get_pronunciation_fails_with_default(self):
        data = Data()
        pronunciation = data.an_ana_dict.get_pronunciation("NOPE", "123")
        self.assertEqual("123", pronunciation)

    def test_deep_copy_when_adding_element(self):
        data = Data()
        word, pronunciations = data.an_entry
        pronunciations = list(pronunciations)
        phon_dict = PhoneticDict({})
        phon_dict[word] = pronunciations

        old_pronunciations = copy.deepcopy(pronunciations)
        pronunciations.append(data.AE_N_AH_pronunciation)
        self.assertEqual(tuple(old_pronunciations), tuple(phon_dict[word]))
        self.assertNotEqual(tuple(pronunciations), tuple(phon_dict[word]))

    def test_clean_strip_whitespace_for_entering_and_lookup(self):
        data = Data()
        word = " \u2002word\u00A0"
        gold = "word"
        dummy_pronunciations = (data.AE_N_pronunciation, ) # Note: semantic nonsense

        data.an_ana_dict[word] = dummy_pronunciations
        self.assertIn(gold, data.an_ana_dict.keys())
        self.assertNotIn(word, data.an_ana_dict.keys())
        self.assertEqual(data.an_ana_dict.get_pronunciations(word), dummy_pronunciations)

    def test_clean_casenorm_for_new_entry_and_lookup(self):
        data = Data()
        word = "WoRD"
        gold = "word"
        dummy_pronunciations = (data.AE_N_pronunciation, ) # Note: semantic nonsense

        data.an_ana_dict[word] = dummy_pronunciations
        self.assertIn(gold, data.an_ana_dict.keys())
        self.assertNotIn(word, data.an_ana_dict.keys())
        self.assertEqual(data.an_ana_dict.get_pronunciations(word), dummy_pronunciations)

    def test_clean_normalise_unicode_accents_for_new_entry_and_lookup(self):
        data = Data()
        a_with_separate_accents = '\u0061\u0301'
        self.assertEqual(len(a_with_separate_accents), 2)  # sanity check
        a_gold = "á"
        self.assertEqual(len(a_gold), 1)  # sanity check

        dummy_pronunciations = (data.AE_N_pronunciation,)  # Note: semantic nonsense
        data.an_ana_dict[a_with_separate_accents] = dummy_pronunciations
        self.assertIn(a_gold, data.an_ana_dict.keys())
        self.assertNotIn(a_with_separate_accents, data.an_ana_dict.keys())
        self.assertEqual(data.an_ana_dict.get_pronunciations(a_with_separate_accents), dummy_pronunciations)


class TestReversePhoneticDict(unittest.TestCase):

    def test_init_valid(self):
        data = Data()
        rev_dict = data.an_ann_reverse_dict
        self.assertEqual(len(rev_dict), 1)
        gold = {data.an_word, data.ann_word}
        self.assertEqual(set(rev_dict[data.AE_N_pronunciation]), gold)

    def test_empty_pronunciation_is_invalid(self):
        data = Data()
        with self.assertRaises(ValueError):
            data.an_ann_reverse_dict[None] = ["What?"]

    def test_empty_words_is_invalid(self):
        data = Data()
        with self.assertRaises(ValueError):
            data.an_ann_reverse_dict[data.AH_N_pronunciation] = None

        with self.assertRaises(ValueError):
            data.an_ann_reverse_dict[data.AH_N_pronunciation] = []

    def test_init_deepcopy_init_data(self):
        data = Data()

        entry = data.AE_N_reverse_entry
        init_data = dict([entry])
        rev_dict = ReversePhoneticDict(init_data)

        old_init_data = copy.deepcopy(init_data)
        init_data[data.AE_N_AH_pronunciation] = ["O no"]
        self.assertEqual(len(rev_dict), len(old_init_data))
        self.assertNotEqual(len(rev_dict), len(init_data))

    def test_deep_copy_when_adding_element(self):
        data = Data()
        pronunciation, words = data.AE_N_reverse_entry
        words = list(words)
        rev_dict = ReversePhoneticDict({})
        rev_dict[pronunciation] = words

        old_words = copy.deepcopy(words)
        words.append("O no")
        self.assertEqual(set(old_words), set(rev_dict[pronunciation]))
        self.assertNotEqual(set(words), tuple(rev_dict[pronunciation]))

    def test_get_words(self):
        data = Data()
        gold = (data.an_word, data.ann_word)
        words = data.an_ann_reverse_dict.get_words(data.AE_N_pronunciation)
        self.assertEqual(words, gold)
        words = data.an_ann_reverse_dict.get_words(data.AE_N_pronunciation, default="who cares")
        self.assertEqual(words, gold)

    def test_get_words_fails_with_default(self):
        data = Data()
        words = data.an_ann_reverse_dict.get_words(data.AE_N_AH_pronunciation, default="123")
        self.assertEqual(words, "123")

    def test_get_word(self):
        data = Data()
        gold = data.an_word
        word = data.an_ann_reverse_dict.get_word(data.AE_N_pronunciation)
        self.assertEqual(word, gold)
        word = data.an_ann_reverse_dict.get_word(data.AE_N_pronunciation, default="Who cares")
        self.assertEqual(word, gold)

    def test_get_word_fails_with_default(self):
        data = Data()
        word = data.an_ann_reverse_dict.get_word(data.AE_N_AH_pronunciation, default="123")
        self.assertEqual(word, "123")

    def test_init_from_phonetic_dict(self):
        data = Data()
        phonetic_dict = data.an_ana_dict
        phonetic_dict[data.ann_word] = (data.AE_N_pronunciation, )  # to have a double pronunciation
        reverse_dict = ReversePhoneticDict.init_from_phonetic_dict(phonetic_dict)
        self.assertEqual(len(reverse_dict), 3)
        self.assertEqual(reverse_dict.get_words(data.AE_N_pronunciation), (data.an_word, data.ann_word ))  # Somewhat dangerous because assumes order is maintained accross conversion
        self.assertEqual(reverse_dict.get_words(data.AH_N_pronunciation), (data.an_word, ))
        self.assertEqual(reverse_dict.get_words(data.AE_N_AH_pronunciation), (data.ana_word, ))


    def test_cannot_add_single_string_as_words_list(self):
        pass
        # TODO


if __name__ == '__main__':
    unittest.main()
