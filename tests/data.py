from phonyface.phoneme import PhonemeType, Phoneme
from phonyface.phonetic_dict import PhoneticDict, ReversePhoneticDict
from phonyface.pronunciation import StressPattern, Stress, Pronunciation


class Data:
    def __init__(self):
        """This data can be used for unit tests.
        Do make sure to never share the same Data object between different unit tests (do not define it in the setUp)
        """
        # DEV NOTE: making changes here might break a lot of tests if you're not careful
        self.STRESS_PATTERN_1 = StressPattern((Stress.MAIN,))
        self.AE = Phoneme("AE", PhonemeType.VOWEL)
        self.AH = Phoneme("AH", PhonemeType.VOWEL)
        self.N = Phoneme("N", PhonemeType.NASAL)
        self.an_word = "an"
        self.AE_N_phonemes = [self.AE, self.N]
        self.AE_N_stresses = self.STRESS_PATTERN_1
        self.AE_N_pronunciation = Pronunciation(phonemes=self.AE_N_phonemes,
                                                stress_pattern=self.AE_N_stresses)
        self.AH_N_phonemes = [self.AH, self.N]
        self.AH_N_stresses = (Stress.NONE,)
        self.AH_N_pronunciation = Pronunciation(phonemes=self.AH_N_phonemes,
                                                stress_pattern=self.AH_N_stresses)
        self.an_entry = (self.an_word, (self.AE_N_pronunciation, self.AH_N_pronunciation))

        self.ana_word = "ana"
        self.AE_N_AH_phonemes = [self.AE, self.N, self.AH]
        self.AE_N_AH_stresses = (Stress.MAIN, Stress.NONE)
        self.AE_N_AH_pronunciation = Pronunciation(phonemes=self.AE_N_AH_phonemes,
                                                   stress_pattern=self.AE_N_AH_stresses)
        self.ana_entry = (self.ana_word, (self.AE_N_AH_pronunciation,))
        self.an_ana_dict_content = dict([self.ana_entry, self.an_entry])
        self.an_ana_dict = PhoneticDict(self.an_ana_dict_content)

        self.ann_word = "ann"
        self.AE_N_reverse_entry = (self.AE_N_pronunciation, (self.an_word, self.ann_word))
        self.an_ann_reverse_dict = ReversePhoneticDict(dict([self.AE_N_reverse_entry]))
