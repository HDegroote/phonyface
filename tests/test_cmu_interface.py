import unittest
import io

from phonyface.interfaces.cmu_interface import CMUDictParser
from tests.data import Data


class TestCMUDict(unittest.TestCase):
    def setUp(self):
        self.mock_phonemes_file = io.StringIO()
        self.mock_phonemes_file.write("AE	vowel\n")
        self.mock_phonemes_file.write("AH	vowel\n")
        self.mock_phonemes_file.write("N	nasal\n")
        self.mock_phonemes_file.seek(0)
        self.phonemes_dict = CMUDictParser.read_cmu_phonemes(self.mock_phonemes_file)

        self.mock_dict_file = io.StringIO()
        self.mock_dict_file.write('an AE1 N\n')
        self.mock_dict_file.write('ana(2) AE1 N AH0\n')
        self.mock_dict_file.seek(0)  # sets stream position to start
        self.cmu_dict = CMUDictParser._read_dict_file(self.mock_dict_file, self.phonemes_dict)


    def test_read_cmu_phonemes(self):
        data = Data()
        self.assertEqual(data.AE, self.phonemes_dict["AE"])
        self.assertEqual(data.AH, self.phonemes_dict["AH"])
        self.assertEqual(data.N, self.phonemes_dict["N"])
        self.assertEqual(len(self.phonemes_dict), 3)

    def test_parse_phonemes_of_word(self):
        data = Data()
        phonemes_in_word = ["AE1", "N", "AH0"]
        parsed_pron = CMUDictParser.parse_pronunciation(phonemes_in_word, self.phonemes_dict)
        self.assertEqual(parsed_pron, data.AE_N_AH_pronunciation)

    def test_create_dict_base_cases(self):
        data = Data()
        self.assertIn(data.AE_N_pronunciation, self.cmu_dict["an"])
        self.assertIn(data.AE_N_AH_pronunciation, self.cmu_dict["ana"])
        self.assertEqual(len(self.cmu_dict), 2)

    def test_create_dict_double_entry(self):
        data = Data()
        self.mock_dict_file = io.StringIO()
        self.mock_dict_file.write('an AE1 N\n')
        self.mock_dict_file.write('an(2) AH0 N\n')
        self.mock_dict_file.seek(0)  # sets stream position to start
        self.cmu_dict = CMUDictParser._read_dict_file(self.mock_dict_file, self.phonemes_dict)

        self.assertIn(data.AE_N_pronunciation, self.cmu_dict["an"])
        self.assertIn(data.AH_N_pronunciation, self.cmu_dict["an"])
        self.assertEqual(len(self.cmu_dict["an"]), 2)
        self.assertEqual(len(self.cmu_dict), 1)

    def test_create_dict_from_file_with_comments(self):
        data = Data()
        self.mock_dict_file = io.StringIO()
        self.mock_dict_file.write('an AE1 N # comment\n')
        self.mock_dict_file.seek(0)
        self.cmu_dict = CMUDictParser._read_dict_file(self.mock_dict_file, self.phonemes_dict)

        self.assertIn(data.AE_N_pronunciation, self.cmu_dict["an"])
        self.assertEqual(len(self.cmu_dict), 1)


if __name__ == '__main__':
    unittest.main()
