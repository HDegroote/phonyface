import itertools
from phonyface.interfaces.cmu_interface import CMUDict
from phonyface.phonetic_dict import ReversePhoneticDict
from phonyface.pronunciation import Stress


def main():

    print("Loading dictionary (this can take a few seconds)")
    cmu_dict = CMUDict()

    print(f"\nNr of entries: {len(cmu_dict)}")

    print("\nLook up look and up: ")
    print(f"\tLook ->", cmu_dict.get_pronunciation("look"))
    print(f"\tUp ->", cmu_dict.get_pronunciation("up"))

    print("\nGet all pronunciations of an example which has more than one:")
    for i, pronunciation in enumerate(cmu_dict.get_pronunciations("an"), 1):
        print(f"\t{i}) 'an' -> {pronunciation}")

    print("\nGet the phoneme type of each phoneme in type")
    types_pronunciation = cmu_dict.get_pronunciation("type")
    for phoneme in types_pronunciation.phonemes:
        print("\t", phoneme, "->", phoneme.kind)

    print("\nGet the word with most pronunciation variations (not unique)")
    word, pronunciations = max(cmu_dict.items(), key=lambda entry: len(entry[1]))
    print(f"The maximal amount of pronunciations for a single word is {len(pronunciations)}")
    print(f"An example is '{word}' with pronunciations: ")
    for i, pronunciation in enumerate(pronunciations, 1):
        print(f"\t{i}) {word} -> {pronunciation}")


    print(f"\n\n{'*' * 30}WORKING WITH REVERSE DICTIONARY{'*' * 30}\n")

    reverse_dict = ReversePhoneticDict.init_from_phonetic_dict(cmu_dict)
    print(f"Amount of entries in the reverse phonetic dictionary: {len(reverse_dict)}")

    print("\nGet entries with the same pronunciation (homophones)")
    pronunciation_same = cmu_dict.get_pronunciation("same")
    homophones = reverse_dict.get_words(pronunciation_same)
    print(f"Only {len(homophones) - 1} entry exists with the same pronunciation. The full set of same homophones consists of:")
    for i, word in enumerate(homophones, 1):
        print(f"\t{i}) {word} [ {pronunciation_same}]")

    print("\nThe largest set of homophones")
    pronunciation, homophones = max(reverse_dict.items(), key=lambda entry: len(entry[1]))
    print(f"The largest set of homophones consists of {len(homophones)} words which can sound like {pronunciation}")
    for i, word in enumerate(homophones, 1):
        print(f"\t{i}) {word} ")


    print(f"\n\n{'*'*20}WORKING WITH STRESS{'*'*20}\n")
    print("(A simple example of an analysis)")
    print("Available stresses:")
    for stress in Stress:
        print("\t", stress, "->", stress.value)

    print("\nGet the stress pattern of pattern")
    stress_pattern_pattern = cmu_dict.get_pronunciation("pattern").stress_pattern
    print(f"\tThe stress pattern of pattern is: {stress_pattern_pattern}")


if __name__ == "__main__":
    main()
